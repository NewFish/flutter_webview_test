if [ ! -d output  ];then
  mkdir output
else
  echo dir exist
fi

rnd=$RANDOM$RANDOM
#rnd="hotfix"
myAlias="my-alias"

#生成随机密码，并保存密码到txt
echo "password:$rnd alias:$myAlias" > password.txt
cat password.txt
mv password.txt output/


#根据密码生成签名文件：key.jks
(echo $rnd && echo $rnd && echo "un" && echo "un" && echo "un" && echo "un" && echo "un"  && echo "un" && echo "y") | keytool -genkey -v -keystore key.jks -keyalg RSA -keysize 2048 -validity 10000 -alias $myAlias
mv key.jks output/

#解压apk
apkName=$(find . -name '*.apk')
unzipApkPath=app-release
java -jar apktool.jar d $apkName -o output/$unzipApkPath

#解压包资源
zipName=$(find . -name '*.zip')
unzip $zipName
zipPath=$(basename $zipName .zip)

#包资源替换
assetsPath=output/$unzipApkPath/assets/flutter_assets/assets
if [ ! -d $assetsPath  ];then
  mkdir $assetsPath
else
  echo dir exist
fi
mv $zipPath/* $assetsPath
rm -r $zipPath

#删除META-INF文件
rm -r output/$unzipApkPath/original/META-INF

#打包apk
java -jar apktool.jar b output/app-release -o output/new-app-release.apk
rm -r output/app-release

#对新包重新签名，生成新包：output.apk
echo $rnd | jarsigner -verbose -keystore output/key.jks -signedjar output/output.apk -digestalg SHA1 -sigalg MD5withRSA output/new-app-release.apk $myAlias
rm output/new-app-release.apk
