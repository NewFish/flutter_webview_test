import 'dart:io';
import 'package:archive/archive.dart';
import 'package:archive/archive_io.dart';
import 'package:path/path.dart' as p;

///解压文件
String _unzipFile(String filePath){

  final file = File(filePath);
  if(!file.existsSync()) return '';
  final fileName = p.basename(file.path);
  final printPath = fileName.split('.').first + '-unzip';


  // Read the Zip file from disk.
  final bytes = file.readAsBytesSync();

  // Decode the Zip file
  final archive = ZipDecoder().decodeBytes(bytes);

  // Extract the contents of the Zip archive to disk.
  for (final file in archive) {
    final filename = file.name;
    if (file.isFile) {
      final data = file.content as List<int>;
      File(p.join(printPath,filename))
        ..createSync(recursive: true)
        ..writeAsBytesSync(data);
    } else {
      Directory(p.join(printPath,filename))
        ..createSync(recursive: true);
    }
  }
  print('$fileName文件解压成功!');
  return printPath;
}

///删除文件
void _deleteDir(Directory dir) async{
  if(dir.existsSync()) {
    List<FileSystemEntity> files = dir.listSync();
    if (files.length > 0) {
      for (var file in files) {
        bool isFile = await FileSystemEntity.isFile(file.path);
        isFile ? file.deleteSync() : await _deleteDir(file);
      }
    }
    dir.deleteSync();
  }
}

///解压apk文件，并进行处理
Future<String> unzipApk(String apkPath) async{
  final outDir =  _unzipFile(apkPath);
  if(outDir.isEmpty){
    print('Error:${p.basename(apkPath)}解压失败');
    return '';
  }
  Directory metaInf = Directory(p.join(outDir, 'META-INF'));
  await _deleteDir(metaInf);
  print('${metaInf.path}目录删除成功');
  return outDir;
}

///解压资源文件
String unzipResource(String zipPath){
  final outDir =  _unzipFile(zipPath);
  if(outDir.isEmpty){
    print('Error:${p.basename(zipPath)}解压失败');
    return '';
  }
  final indexPath = p.join(outDir, 'index.html');
  File indexFile = File(indexPath);
  if(!indexFile.existsSync()){
    print('Error:$outDir目录未找到 index.html 文件,请规范压缩文件目录！}');
    return '';
  }
  return outDir;
}

///将资源文件放入解压后的apk
void moveFile(String apkDir, String zipDir) async{
  Directory zip = Directory(zipDir);
  final assetPath = p.join(apkDir, 'assets', 'flutter_assets', 'assets',);
  final assetDir = Directory(assetPath);
  if(!assetDir.existsSync()) assetDir.createSync();
  zip.renameSync(assetPath);
  print('资源文件迁移成功，即将重新打包');
  await _deleteDir(Directory(zipDir));
  print('${zipDir}目录成功清除');
}

///对资源文件重新打包，并清除之前的解压内容
void zipApk(String apkDir, String originApkPath) async{
  var encoder = ZipFileEncoder();
  final outApkName = 'new-' + p.basename(originApkPath);
  final outPutApk = File(outApkName);
  if(outPutApk.existsSync()) outPutApk.deleteSync();
  encoder.zipDirectory(Directory(apkDir), filename: outApkName);
  _deleteDir(Directory(apkDir));
  print('${apkDir}目录成功清除');
  print('重新打包成功,生成新apk:$outApkName');

}