
import 'dart:io';
import 'package:path/path.dart';

import 'unzipFile.dart';


void main(List<String> argument) async{
  if(argument == null || argument.length < 2) {
    print('Error:请输入要解压的apk及要替换的资源包\n参考:dart unzip.dart xxx.apk xxx.zip');
    return;
  };
  File apkFile = File(argument[0]);
  File zipFile = File(argument[1]);
  if(!apkFile.existsSync()){
    print('Error:${argument[0]}文件不存在');
    return;
  }
  if(!zipFile.existsSync()){
    print('Error:${argument[1]}文件不存在');
    return;
  }
  final apkName = basename(apkFile.path).substring(0,basename(apkFile.path).indexOf('.'));
  print(apkName);

//  final apkUnzipDir = await unzipApk(apkFile.path);
  final zipUnzipDir = unzipResource(zipFile.path);
//  if(apkUnzipDir.isEmpty || zipUnzipDir.isEmpty) return;
  moveFile(apkName, zipUnzipDir);
//  zipApk(apkUnzipDir, apkFile.path);

}
//dart ../unzip.dart app-release.apk dist.zip