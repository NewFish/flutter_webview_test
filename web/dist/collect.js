/**
 * 收集资源加载错误、js语法错误
 */
(function() {
  let key = '',
      appId = '',
      cssErrorList = [],
      jsErrorList = [],
      // imgErrorList = [],
      jsSyntaxErrorList = []

  window.addEventListener('load', function(e) {
    let scripts = document.getElementsByTagName('script')
    key = scripts[0].dataset.key
    appId = scripts[0].dataset.appid
    // sendData('http://test.com', {cssErrorList, jsErrorList, imgErrorList, jsSyntaxErrorList})
    sendData('http://test.com', {cssErrorList, jsErrorList, jsSyntaxErrorList})
  }, true)

  window.addEventListener('error',function(e){
    decetionError(e)
  }, true)

  /**
   * 检查错误
   * @param {e} e 事件
   */
  function decetionError(e) {
    console.log(e)
    let typeName = e.target.localName
    if(typeName) {
      switch(typeName) {
        case 'link':
          cssErrorList.push({
            type: 'css',
            url: e.target.href,
          })
          break
        case 'script':
          jsErrorList.push({
            type: 'script',
            url: e.target.src,
          })
          break
        // case 'img':
        //   imgErrorList.push({
        //     type: 'img',
        //     url: e.target.src,
        //   })
        //   break
        default:
          break
      }
    }else {
      if (e.message && e.filename) {
        jsSyntaxErrorList.push({
          type: 'syntax',
          details: {
            message: e.message,
            line: e.lineno,
            column: e.colno,
            errorObject: e.error,
          }
        })
      }
    }
  }

  /**
   * 发送请求到服务器
   * @param {String} url 接收错误信息的url地址
   * @param {Object} data 收集的错误信息
   */
  function sendData(url, data) {
    data.page = window.location.href
    data.userInfo = navigator.appVersion
    data.userAgent = navigator.userAgent
    console.log('发送数据', url, data)
    // return new Promise( function( resolve, reject) {
    //   let xhr = new XMLHttpRequest()
    //   xhr.responseType = 'json'
    //   xhr.open('post', url, data)
    //   xhr.setRequestHeader('key', key)
    //   xhr.setRequestHeader('appId', appId)
    //   xhr.onload = function () {
    //     resolve(xhr.response)
    //   }
    //   xhr.onerror = reject
    //   xhr.send(data)
    // })
  }

  function IdentityCheck() {
    // key,appId校验
  }

})()
