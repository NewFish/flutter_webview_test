import 'dart:io';
import 'package:archive/archive.dart';
import 'package:archive/archive_io.dart';
import 'package:path/path.dart' as p;

String unzipFile(String filePath){

  final file = File(filePath);
  if(!file.existsSync()) return '';
  final printPath = p.dirname(filePath);
  final uzPathResult = p.join(printPath, 'index.html');
  final uzFile = File(uzPathResult);
  if(uzFile.existsSync()) return uzPathResult;

  // Read the Zip file from disk.
  final bytes = file.readAsBytesSync();

  // Decode the Zip file
  final archive = ZipDecoder().decodeBytes(bytes);

  // Extract the contents of the Zip archive to disk.
  for (final file in archive) {
    final filename = file.name;
    if (file.isFile) {
      final data = file.content as List<int>;
      File(p.join(printPath,filename))
        ..createSync(recursive: true)
        ..writeAsBytesSync(data);
    } else {
      Directory(p.join(printPath,filename))
        ..create(recursive: true);
    }
  }
  return uzPathResult;
}