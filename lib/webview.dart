import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:webview_flutter/webview_flutter.dart';

class MyWebView extends StatefulWidget {
  @override
  _MyWebViewState createState() => _MyWebViewState();
}

class _MyWebViewState extends State<MyWebView> {
  // final Completer<WebViewController> _contrl = Completer<WebViewController>();
  WebViewController _webcontrl ;
  _loadHtml() async {
    // String path = 'lib/wk.html';
    String path = 'web/dist/index.html';
    String dataStr = await rootBundle.loadString(path);
    _webcontrl.loadUrl(
      Uri.dataFromString(dataStr,mimeType: 'text/html',encoding: Encoding.getByName('utf-8')).toString()
    );
  }
  JavascriptChannel _toasterJavascriptChannel(BuildContext context) {
    return JavascriptChannel(
        name: 'Toaster',
        onMessageReceived: (JavascriptMessage message) {
          Scaffold.of(context).showSnackBar(
            SnackBar(content: Text(message.message)),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('加载Web'),
      ),
      body: WebView(
        initialUrl: "https://www.baidu.com",
        javascriptMode: JavascriptMode.unrestricted,
        javascriptChannels: <JavascriptChannel>[
            _toasterJavascriptChannel(context),
          ].toSet(),
          navigationDelegate: (NavigationRequest request) {
            if (request.url.startsWith('https://www.youtube.com/')) {
              print('blocking navigation to $request}');
              return NavigationDecision.prevent;
            }
            print('allowing navigation to $request');
            return NavigationDecision.navigate;
          },
        onWebViewCreated: (ctl) {
          print("onWebViewCreated-----");
          // _contrl.complete(ctl);
          _webcontrl = ctl;
          //异步加载
          _loadHtml();
        },
        onPageStarted: (str) {
          print('onPageStarted----');
        },
        onPageFinished: (str) {
          print('onPageFinished----');
        },
      ),
    );
  }
}
