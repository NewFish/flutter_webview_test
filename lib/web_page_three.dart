import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:only_test/utils/download_util.dart';
import 'package:only_test/utils/unzip_util.dart';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart' as p;
import 'package:webview_flutter/webview_flutter.dart';
import 'package:fluttertoast/fluttertoast.dart';

class WebPageThree extends StatefulWidget {
  @override
  _WebPageThreeState createState() => _WebPageThreeState();
}

class _WebPageThreeState extends State<WebPageThree> {
  String _urlPath = 'assets/index.html';
  int progress = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: _urlPath == null ? getDownloadWidget() : buildWebView(),
    );
  }

  Widget getDownloadWidget() {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          FlatButton(
            onPressed: () => downloadFile(
                'https://work-1256696029.cos.ap-guangzhou.myqcloud.com/webview_test/dist.zip'),
            child: Text(
              '点击下载',
              style: TextStyle(
                color: Colors.white,
              ),
            ),
            color: Theme.of(context).primaryColor,
          ),
          Text('下载进度:%$progress')
        ],
      ),
    );
  }

  WebView buildWebView() {
    return WebView(
      onWebViewCreated: (WebViewController controller) {
//        controller.loadLocalHtmlFile(_urlPath);
        controller.loadAssetHtmlFile(_urlPath);
        debugPrint('onWebViewCreated成功');
      },
      javascriptMode: JavascriptMode.unrestricted,
    );
  }

  Future<void> downloadFile(String url) async {
    String path = (await getApplicationDocumentsDirectory()).path;
    String fileName = url.substring(url.lastIndexOf("/") + 1);
    String filePath = p.join(path, fileName);
    File file = File(filePath);
    if (file.existsSync()) {
      showToast('文件已存在，无需重新下载');
      uzFile(filePath);
      return;
    }
    Dio().download(url, filePath,onReceiveProgress: (rec, total) {
      progress = ((rec / total) * 100).floor();
      setState(() {});
      if (progress >= 100) {
        debugPrint('下载完成');
        showToast('开始解压');
        uzFile(filePath);
      }
    });
  }

  void uzFile(String filePath) {
    final unzipPath = unzipFile(filePath);
    if (unzipPath.isNotEmpty) {
      debugPrint('解压成功:$unzipPath');
      showToast('解压成功，开始加载');
      File file = File(filePath);
      if(file.existsSync()) file.delete();
      _urlPath = unzipPath;
      setState(() {});
    }
  }

  void showToast(String message){
    Fluttertoast.showToast(
        msg: message,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIos: 1,
        backgroundColor: Colors.red,
        textColor: Colors.white,
        fontSize: 16.0
    );
  }

}
