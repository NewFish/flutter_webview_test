

import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

class WebPageTwo extends StatefulWidget {
  @override
  _WebPageTwoState createState() => _WebPageTwoState();
}

class _WebPageTwoState extends State<WebPageTwo> {
  WebViewController _controller;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: buildWebView(),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: FloatingActionButton(
        onPressed: () async{
          final result = await _controller.evaluateJavascript('flutterCall("我来自android")');
          showMessage(result);
        },
        child: Icon(Icons.touch_app),
      ),
    );
  }

  void showMessage(String message){
    _scaffoldKey.currentState.showSnackBar(SnackBar(content: Text(message,textAlign: TextAlign.center,)));
  }

  WebView buildWebView() {
    return WebView(
      initialUrl: "assets/dist/index.html",
      onWebViewCreated: (WebViewController controller) {
        _controller = controller;
        debugPrint('onWebViewCreated成功');
      },
      javascriptMode: JavascriptMode.unrestricted,
      javascriptChannels: {
        JavascriptChannel(
          name: 'flutter',
          onMessageReceived: (JavascriptMessage message) {
            print('收到来自js:${message.message}');
            showMessage(message.message);
          },
        ),
      },
    );
  }
}
