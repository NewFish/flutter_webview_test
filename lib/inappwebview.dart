import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';

class MyWebInApp extends StatefulWidget {
  @override
  _MyWebInAppState createState() => _MyWebInAppState();
}

class _MyWebInAppState extends State<MyWebInApp> {
  InAppWebViewController _webcontrl;
  String target = "还没写";
  double progress = 0;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('加载Web'),
        ),
        body: Column(
          children: <Widget>[
            Expanded(
              flex: 3,
              child: InAppWebView(
                // initialUrl: 'www.baidu.com',
                initialFile: "web/dist/wk.html",
                initialHeaders: {},
                initialOptions: InAppWebViewWidgetOptions(),
                onWebViewCreated: (InAppWebViewController controller) {
                  _webcontrl = controller;
                },
                onLoadStart: (InAppWebViewController controller, String url) {
                  print('onLoadStart');
                },
                onLoadStop:
                    (InAppWebViewController controller, String url) async {
                  print('onLoadStop');
                  // setState(() {
                  //   this.url = url;
                  // });
                  _webcontrl.addJavaScriptHandler(
                      handlerName: "mobileHandler",
                      callback: (arg) {
                        print('---------------');
                        print(arg);
                        return '888';
                      });
                },
                onProgressChanged:
                    (InAppWebViewController controller, int progress) {
                  print('进度---$progress');
                  setState(() {
                    this.progress = progress / 100;
                  });
                },
              ),
            ),
            Container(
              height: 1,
              color: Colors.grey,
            ),
            Expanded(
                child: Container(
              color: Colors.yellowAccent[100],
              child: Column(
                children: <Widget>[
                  //目标数字
                  Center(
                    child: Text(
                      '目标数字：$target',
                      style: TextStyle(
                          fontSize: 20.0,
                          color: Colors.indigoAccent,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  Row(
                    children: <Widget>[
                      RaisedButton.icon(
                        onPressed: () {
                          print('one');
                          _webcontrl
                              .evaluateJavascript(
                                  source: 'nativeGetShare(\'88\')')
                              .then((arg) {
                            print("web回调---$arg");
                          });
                        },
                        icon: Icon(Icons.flash_on),
                        label: Text('调Js'),
                      ),
                      RaisedButton.icon(
                        onPressed: () {
                          print('two');
                          _webcontrl
                              .evaluateJavascript(
                                  source: "flutterCall(\'hello\')")
                              .then((arg) {
                            print("web回调---$arg");
                          });
                        },
                        icon: Icon(Icons.stars),
                        label: Text('按钮'),
                      ),
                    ],
                  ),
                ],
              ),
            ))
          ],
        ));
  }
}
