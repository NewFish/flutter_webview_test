
Project_Name="Runner"

Configuration="Release"

Time=$(date "+%Y%m%d-%H%M%S")
TargetFile="archive"
BUILD_PATH=./app/app${Time}

xcodebuild archive -workspace ./${Project_Name}.xcworkspace -scheme ${Project_Name} -configuration ${Configuration} -archivePath ./${TargetFile}

xcodebuild -exportArchive -exportOptionsPlist ExportOptions.plist -archivePath ./${TargetFile}.xcarchive -exportPath ${BUILD_PATH} -allowProvisioningUpdates



